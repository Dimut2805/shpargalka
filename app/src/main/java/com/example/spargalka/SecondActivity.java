package com.example.spargalka;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((WebView) findViewById(R.id.webView))
                .loadUrl(
                        getResources()
                                .getStringArray(R.array.stringArray)
                                [
                                getIntent()
                                        .getIntExtra("list", 0)
                                ]
                );

    }
}
